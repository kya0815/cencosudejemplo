let ERROR_GENERICO=404;

function CustomError( msg ,error, cod) {
    this.cod = cod || ERROR_GENERICO;
    this.message = msg || '';
    this.error_detail = error;
    return this;
}
CustomError.prototype = Object.create( TypeError.prototype );
CustomError.prototype.constructor = CustomError;

module.exports= CustomError;
