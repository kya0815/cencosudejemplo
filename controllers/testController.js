const ResError = require('../schema/model/resultErrorModel');
const resultOK = require('../schema/model/resultOkModel');

exports.hotelier = function(req, res,next) {
    try{
        if (req.body) {
            if (req.body.quantity === req.body.events.length && eventValid(req.body.events)) { // validcion que la cantidad sea igual al string de eventos
                let result= [0,0,0,0,0,0,0,0,0,0]; // Base de matriz para respuesta
                let re = /\d/; // REGEX para busqueda de un digito
                req.body.events.split('').map( // a partir de la cadena de eventos que mandan se crea un lista y se intera
                    event => {
                        if (re.exec(event)) { // se valida si es un numero
                            result[parseInt(event)] = 0;
                        } else {
                            if (event === 'L') { // se valida si es L
                                let pos = result.findIndex(value => value === 0); // busca el primero que coincida
                                if (pos !== -1) {
                                    result[pos] = 1;
                                }
                            } else if (event === 'R') { // se valida si es R
                                result.reverse();
                                let pos = result.findIndex(value => value === 0);
                                if (pos !== -1) {
                                    result[pos] = 1;
                                }
                                result.reverse();
                            }
                        }
                    }
                );
                return res.status(200).json(resultOK.newResultaData(200,"data process", result));
            } else {
                next(new ResError("information does not match",new Error()));
            }
        }else{
            next(new ResError("one of the required fields is not correct or is not sent",new Error()));
        }
    }catch (e){
        next(new ResError("Invalid Data",e));
    }
};


function eventValid(events) { // validador de formato de eventos
    return events.replace (/[0-9]+/g, '').replace (/[LR]+/g, '').length === 0;
}
