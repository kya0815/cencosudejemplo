exports.new = function (mensajeServer,mensajeUsuario) {
    return {
        "result": mensajeServer,
        "message": mensajeUsuario
    };
};
exports.newResultaData = function (mensajeServer,mensajeUsuario,data) {
    return {
        "result": mensajeServer,
        "message": mensajeUsuario,
        "data":     data
    };
};
