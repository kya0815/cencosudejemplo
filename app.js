const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const testRoute = require('./routes/testRoute');
const resError = require('./schema/model/resultErrorModel');

const app = express();

app.use(bodyParser.json({limit: "50mb"}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/test', testRoute);
app.use(logErrors);
function logErrors(err, req, res, next) {
    try{
        console.error(err);
    }catch (e){
    }
    next(err);
}
app.use(function (req, res, next) {
    const err = new Error("Api no Soprotada");
    err.status = 404;
    next(new resError("Api no Soprotada",err));
});
app.use(function (err, req, res, next) {
    res.status(err.cod || err.status || 500)
        .json(err);
});

module.exports = app;
